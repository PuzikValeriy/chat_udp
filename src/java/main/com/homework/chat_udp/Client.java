package com.homework.chat_udp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.UUID;

/**
 * Created by Valeriy on 06.02.2017.
 */
public class Client {
        public static void main(String[] args) {
            String uuid = UUID.randomUUID().toString();
            Thread write = new Thread(() -> {
                try (MulticastSocket socket = new MulticastSocket(4444);
                    BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {
                    InetAddress group = InetAddress.getByName("228.5.6.7");
                    socket.joinGroup(group);
                    while (true) {
                        String line = stdIn.readLine()+uuid;
                        byte[] buffer = line.getBytes();
                        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, group, 4444);
                        socket.send(packet);
                        if (new String(packet.getData(),0,packet.getLength()-36).equals("exit")) {
                            System.exit(0);
                            break;}
                    }
                    socket.leaveGroup(group);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            Thread read = new Thread(() -> {
                try (MulticastSocket socket = new MulticastSocket(4444)) {
                    InetAddress group = InetAddress.getByName("228.5.6.7");
                    socket.joinGroup(group);
                    byte[] buffer = new byte[256];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    while (true) {
                        socket.receive(packet);
                        int position=packet.getLength()-36;
                        if(!new String(packet.getData(), position, packet.getLength()-position).equals(uuid))
                            System.out.println(new String(packet.getData(), 0, position));
                        if (new String(packet.getData(),0,position).equals("exit")) {
                            System.exit(0);
                            break;}
                    }
                    socket.leaveGroup(group);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            read.start();
            write.start();
        }
    }
